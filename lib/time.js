let time = {
  "result":[
    {
      "name" : '全部',
      'month':[
        {
          name:'--',
          id:'all'
        }
      ]
    },
    {
      "name": '2019',
      'month': [
        {
          "name": '01',
        },
        {
          "name": '02',
        },
        {
          "name": '03',
        },
        {
          "name": '04',
        },
        {
          "name": '05',
        },
         {
          "name": '06',
        },
        {
          "name": '07',
        },
        {
          "name": '08',
        },
        {
          "name": '09',
        },
        {
          "name": '10',
        },
        {
          "name": '11',
        },
        {
          "name": '12',
        },

      ]
    },
     {
      "name": '2018',
      'month': [
        {
          "name": '01',
        },
        {
          "name": '02',
        },
        {
          "name": '03',
        },
        {
          "name": '04',
        },
        {
          "name": '05',
        },
        {
          "name": '06',
        },
        {
          "name": '07',
        },
        {
          "name": '08',
        },
        {
          "name": '09',
        },
        {
          "name": '10',
        },
        {
          "name": '11',
        },
        {
          "name": '12',
        },

      ]
    },
    {
      "name": '2017',
      'month': [
        {
          "name": '01',
        },
        {
          "name": '02',
        },
        {
          "name": '03',
        },
        {
          "name": '04',
        },
        {
          "name": '05',
        },
        {
          "name": '06',
        },
        {
          "name": '07',
        },
        {
          "name": '08',
        },
        {
          "name": '09',
        },
        {
          "name": '10',
        },
        {
          "name": '11',
        },
        {
          "name": '12',
        },

      ]
    }
  ]

}
module.exports = time;