// return a new Array 
function getnewArray(data,key){
  let array = [];
  data.forEach(element => {
    array.push(element[key])
  });
  return array
}
export {
  getnewArray
}