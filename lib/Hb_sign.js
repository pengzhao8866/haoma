import {
  hexMD5
} from "./md5.js"
import { isIos } from '../utils/util'
import regeneratorRuntime from '../lib/regenerator-runtime'
// #ifdef H5
import { ifweixin } from '../utils/h5_util'
// #endif

// uni.getStorageSync("isIos")

let Hb_signRandom = () => {
  let d = 0;
  if (uni.getStorageSync('random')) {
    d = uni.getStorageSync('random');

  } else {
    d = Math.random();
    uni.setStorageSync('random', d)
  }

  let port = uni.getStorageSync("isIos") ? true : false;
  // port = true

  const Hb_sign = {
    // v: '1.2.2',
    v: '1.4.1',
    t: new Date().getTime(),
    d
  }

  // #ifdef H5
  Hb_sign.c = ifweixin() ? 'HB_H5_WX_WAP_USER' : 'HB_H5_WAP_USER';
  // #endif

  // #ifndef H5
  Hb_sign.c = port ? "HB_MINI_IOS_USER" : "HB_MINI_ANDROID_USER";
  // #endif

  return [hexMD5(Hb_sign.c + Hb_sign.t + Hb_sign.v), Hb_sign]
}

export default Hb_signRandom;