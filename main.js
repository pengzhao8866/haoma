import Vue from 'vue';
import App from './App';
import './js_sdk/ican-H5Api/ican-H5Api';

//  #ifdef H5
// import AMap from "vue-amap"; //地图
// 	AMap.initAMapApiLoader({
// 				// 申请的高德key
// 				key: "ab204c36b93b6cdadc841fcf07e54cc1",
// 				// 插件集合
//     plugin: [
//           "Geolocation",
// 				]
// });
// #endif

// import tabbar from "./components/tabbar/tabbar";
// Vue.component('Tabbar', tabbar);

import headNav from "./components/headNav/headNav";
Vue.component('headNav', headNav);
//  #ifdef H5
import Login from "./components/login/login";
Vue.component('Login', Login);
// #endif

Vue.config.productionTip = false;

//  #ifdef H5
Vue.directive('focus', {
	bind(el, option) {

		if (option.value) {
			el.getElementsByTagName('input')[0].focus()
		} else {
			el.getElementsByTagName('input')[0].blur()
		}

	},
	updated(el, option) {
		if (option.value) {
			el.getElementsByTagName('input')[0].focus()
		} else {
			el.getElementsByTagName('input')[0].blur()
		}

	},
	inserted(el, option) {
		if (option.value) {
			el.getElementsByTagName('input')[0].focus()
		} else {
			el.getElementsByTagName('input')[0].blur()
		}

	},
	componentUpdated(el, option) {
		if (option.value) {
			el.getElementsByTagName('input')[0].focus()
		} else {
			el.getElementsByTagName('input')[0].blur()
		}
		var u = navigator.userAgent;
		if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
			setInterval(function () {
				el.getElementsByTagName('input')[0].scrollIntoView(false);
			}, 200)
		}

	}
})
// #endif

App.mpType = 'app';

const app = new Vue({
	...App
});
app.$mount();
