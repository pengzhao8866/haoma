import regeneratorRuntime from '../lib/regenerator-runtime'
import request from '../utils/request'
const phoneRegister = e => {

  if ("getPhoneNumber:ok" != e.errMsg) {
    uni.showToast({
      icon: 'none',
      title: '信息获取失败,请重新登录'
    })
    return;
  }

  let promise = new Promise((resolve, reject) => {

    let data = {
      sessionKey: uni.getStorageSync('session_key'),
      encryptedData: e.encryptedData,
      iv: e.iv
    }
    //     console.log(data)
    // return ;
    if (!data.encryptedData) {
      return;
    }
    request.handlerPost('account/weChat/wechatLogin/v2', data, "POST").then(res => {
      if (!res.success) {

        request.getSession_key();
      }

      if (res.data.dueTime) {
        res.data.dueTime = res.data.dueTime.replace(/\-/g, "/")
        res.data.dueTime = new Date(res.data.dueTime).Format("yyyy.MM.dd")
      }
      if (res.data.state != 3) {
        uni.$emit('tabbarUpdate', { msg: 'tabbar更新' })
      }

      uni.setStorageSync('userInfo', res.data);
      if (res.data.state == 2) {
        uni.setStorageSync('isMember', true);
      }
      uni.setStorageSync('token', res.data.token);
      resolve(res);
    })

  })

  return promise;
}



export {
  phoneRegister
}