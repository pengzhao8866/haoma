import request from "./request";
//  #ifdef H5
import wxapi from "./wxapi.js";
var app = getApp();
// #endif

const getPosition = callback => {
  igetLocation.init("_BROWER");
};

// 微信获得经纬度
const getlnglat = async (type = 1, isReLocate = false, isNeedGps = false) => {
  let lnglat = {};
    // #ifdef  APP-PLUS
  let obj = {};
    obj.cityId = uni.getStorageSync("city").cityId || 87;
    obj.latitude = 30.28;
    obj.longitude = 120.15;
  return obj;
    // #endif
  
  //  #ifdef H5
  if (isNeedGps) {
    lnglat = (await H5getUserLocation(type, isReLocate)) || "";
  } else {
    if (uni.getStorageSync("latitude")) {

      lnglat.longitude = uni.getStorageSync("longitude");
      lnglat.latitude = uni.getStorageSync("latitude");
      if (type == 2) {
        uni.setStorageSync("city", uni.getStorageSync("nowCity"))
      }
      lnglat.cityId = uni.getStorageSync("city").cityId;

    } else {
      lnglat = (await H5getUserLocation(type, isReLocate)) || "";
    }
  }
  
  if (lnglat && lnglat.longitude) {
    return {
      longitude: lnglat.longitude,
      latitude: lnglat.latitude,
      cityId: lnglat.cityId
    };
  } else {
    let obj = {};
    obj.cityId = uni.getStorageSync("city").cityId || 87;
    obj.latitude = 30.28;
    obj.longitude = 120.15;
    return obj;
  }
  // #endif

  //  #ifndef H5
  if (isNeedGps) {
    lnglat = (await getUserLocation(type, isReLocate)) || "";
  } else {
    if (uni.getStorageSync("latitude")) {
      lnglat.longitude = uni.getStorageSync("longitude");
      lnglat.latitude = uni.getStorageSync("latitude");
      lnglat.cityId = uni.getStorageSync("city").cityId;
    } else {
      lnglat = (await getUserLocation(type, isReLocate)) || "";
    }
  }
  if (lnglat) {
    return {
      longitude: lnglat.longitude,
      latitude: lnglat.latitude,
      cityId: lnglat.cityId
    };
  } else {
    let obj = {};
    obj.cityId = uni.getStorageSync("city").cityId || 87;
    obj.latitude = 30.28;
    obj.longitude = 120.15;
    return obj;
  }
  // #endif
};

//  #ifndef H5
const getUserLocation = async (type, isReLocate) => {
  return new Promise((resolve, reject) => {
    uni.getSetting({
      success: async res => {
        // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
        // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
        // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
        if (
          res.authSetting["scope.userLocation"] != undefined &&
          res.authSetting["scope.userLocation"] != true &&
          isReLocate
        ) {
          uni.openSetting({
            success: async dataAu => {
              if (dataAu.authSetting["scope.userLocation"] == true) {
                //再次授权，调用uni.getLocation的API
                resolve(await getLocation(type));
              } else {
                uni.showToast({
                  title: "您已拒绝授权",
                  icon: "none",
                  duration: 1000
                });
                if (uni.getStorageSync("nowCity")) {
                  uni.removeStorageSync("nowCity");
                }
                resolve("");
              }
            }
          });
        } else if (res.authSetting["scope.userLocation"] == undefined) {
          //调用uni.getLocation的API
          resolve(await getLocation(type));
        } else {
          //调用uni.getLocation的API
          resolve(await getLocation(type));
        }
      }
    });
  });
};

const getLocation = async type => {
  return new Promise((resolve, reject) => {
    uni.getLocation({
      type: "wgs84",
      success: async res => {
        uni.setStorage({
          key: "latitude",
          data: res.latitude
        });
        uni.setStorage({
          key: "longitude",
          data: res.longitude
        });
        if (!uni.getStorageSync("city") || type > 1) {
          let city = await getUserCity(res);
          uni.setStorageSync("nowCity", city);
          if (type == 2 || !uni.getStorageSync("city")) {
            uni.setStorageSync("city", city);
          }
          res = Object.assign(res, city);
        } else {
          res = Object.assign(res, uni.getStorageSync("city"));
        }
        resolve(res);
      },
      fail: res => {
        uni.getSetting({
          success: res => {
            if (!res.authSetting["scope.userLocation"]) {
            } else {
              //用户已授权，但是获取地理位置失败，提示用户去系统设置中打开定位
              uni.showModal({
                title: "和权益提醒您",
                content: "为了给您更好的服务，请在系统设置中打开定位服务",
                confirmText: "确定"
              });
            }
          }
        });

        if (uni.getStorageSync("nowCity")) {
          uni.removeStorageSync("nowCity");
        }
        resolve("");
      }
    });
  });
};

// #endif

// h5端（含微信端）获取位置定位

//  #ifdef H5
const H5getUserLocation = async (type, isReLocate) => {
  let res = {};
  if (app.globalData.ifweixin) {
    if (isReLocate) {
      res = await wxapi.wxRegister(wxapi.getLocation);
    }
  } else {
    if (!uni.getStorageSync('city') || isReLocate) {
      res = await H5getLocation()
    }
  }


  if (res.latitude) {
    uni.setStorage({
      key: "latitude",
      data: res.latitude
    });
    uni.setStorage({
      key: "longitude",
      data: res.longitude
    });
    if (!uni.getStorageSync("city") || type > 1) {
      let city = await getUserCity(res)
      uni.setStorageSync("nowCity", city);
      if (type == 2 || !uni.getStorageSync("city")) {
        uni.setStorageSync("city", city);
      }
      res = Object.assign(res, city);
    } else {
      if (type == 1 && !app.globalData.ifweixin) {
        let city = await getUserCity(res);
        uni.setStorageSync("nowCity", city);
        if (!uni.getStorageSync("city")) {
          uni.setStorageSync("city", city);
        }
      }
      res = Object.assign(res, uni.getStorageSync("city"))
    }
  }
  else {
    if (type == 2) {
      uni.showToast({
        title: '您可能关闭了定位，请刷新网页重试',
        icon: 'none',
        image: '',
        duration: 1500,
      });
    }
  }

  return new Promise((resolve, reject) => {
    resolve(res);
  });
};

//非微信浏览器端的gps
const H5getLocation = () => {
  let falg = false;
  setTimeout(() => {
    if (!falg) {
      uni.showLoading({
        title: "获取您的定位中,请耐心等待"
      });
    }
  }, 1000);

  return new Promise((resolve, reject) => {
    let geolocation1 = new qq.maps.Geolocation("BYTBZ-VC6LX-OCH4F-7ZWPR-KDY4Z-CTFKV", "myapp");
    geolocation1.getLocation(res => {
      falg = true;
      uni.hideLoading();
      let res1 = {};
      res1.longitude = res.lng;
      res1.latitude = res.lat;
      // resolve(res1)

      // 转为wgs-84定位
      request.handlerPost('iceberg/home/app/mars/fixPosition', res1).then(res => {
        resolve(res.data)
      })

    }, res => {
      falg = true;
      uni.hideLoading();
      if (uni.getStorageSync("nowCity")) {
        uni.removeStorageSync("nowCity");
      }
      resolve('')
    }, {
      enableHighAccuracy: true,
      timeout: 3000,
      maximumAge: 0
    }
    );
  })
}

//h5打开地图

// #endif
//得到用户所在城市
const getUserCity = async query => {
  let res = await request.handlerPost("iceberg/home/app/fixPosition", query);
  const city = { cityId: res.data.cityId, cityName: res.data.cityName };
  addCityList(city);
  return city;
};

//前往内置地图
const goaddress = res => {
  //  #ifdef H5
  if (app.globalData.ifweixin) {
      wxapi.wxRegister(
      wxapi.openLocation({
        ...res,
        scale: 18
      })
    );
  } else {
    let itemList = ["高德地图", "百度地图", "腾讯地图"];
    let url = ''
    uni.showActionSheet({
      itemList,
      success: res1 => {
        switch (res1.tapIndex) {
          case 0:
            url = `https://uri.amap.com/marker?position=${res.longitude},${res.latitude}&name=${res.name}&coordinate=wgs84`
            break;
          case 1:
            url = `http://api.map.baidu.com/marker?location=${res.latitude},${res.longitude}&title=${res.name}&content=${res.address}&output=html&coord_type=wgs84&output=html&src=webapp.baidu.openAPIdemo`

            break;
          case 2:
            url = `http://apis.map.qq.com/uri/v1/marker?marker=coord:${res.latitude},${res.longitude};title:${res.name};addr:${res.address}&coord_type=1&referer=myapp`
            break;
          default:
            url = `http://uri.amap.com/marker?position=${res.longitude},${res.latitude}&name=${res.name}&coordinate=wgs84`
            break;
        }
        if (app.globalData.isIos1 && res1.tapIndex > 0) {
          window.location.href = url
        } else {
          url = encodeURIComponent(url)
          wx.navigateTo({
            url: `../h5Page/h5Page?h5Link=${encodeURIComponent(url)}&title=位置·${itemList[res1.tapIndex]}&isencode=true`
          });
        }

      }
    });
  }
  //  #endif
  //  #ifndef H5
  uni.openLocation({
    ...res,
    scale: 18
  });
  //  #endif
};

//判断是否定位地址是当前城市
const isCitySame = () => {
  if (!uni.getStorageSync("nowCity")) {
    return true;
  }
  if (
    uni.getStorageSync("city").cityId == uni.getStorageSync("nowCity").cityId
  ) {
    return true;
  } else {
    return false;
  }
};

//存储定位信息
const addCityList = obj => {
  let cityList = uni.getStorageSync("cityList") || [];
  if (cityList.length > 0) {
    cityList = cityList.filter(element => {
      return element.cityId != obj.cityId;
    });
    if (cityList.length == 3) {
      cityList.pop();
    }
    cityList.unshift(obj);
  } else {
    cityList.push(obj);
  }
  uni.setStorageSync("cityList", cityList);
};

export { getlnglat, goaddress, isCitySame, addCityList };
