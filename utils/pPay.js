/*
    微信支付方法(uni-app h5)适用
    获取微信加签信息
    @param{data}:获取的微信加签
    @param{res}:成功回调
    @param{fail}:失败回调
     
    @warn:因为package为严格模式下的保留字，不能用作变量.
    @use
     
        wPay({
            appId,
            timeStamp,
            nonceStr,
            signature,
            package,
            paySign
        },res=>{
            console.log('调用成功!');
        },fail=>{
            console.log('调用失败!');
        })
*/
const jweixin = require('jweixin-module');
const wexinPay = (data, cb, errorCb, in1) => {

    let [appId, timestamp, nonceStr, packages, paySign, signType] = [data.appId, data.timeStamp, data.nonceStr,
    data.pack, data.paySign, data.signType
    ];


    jweixin.config({
        appId, // 必填，公众号的唯一标识
        timestamp, // 必填，生成签名的时间戳
        nonceStr, // 必填，生成签名的随机串
        signature: data.paySign, // 必填，签名，见附录1
        jsApiList: ['chooseWXPay'], // 必填，需要使用的JS接口列表，所有JS接口列表见附录2,
        // debug: true
    });

    jweixin.ready(function () {
        if (in1) {
            in1()
        }
        jweixin.chooseWXPay({
            timestamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
            nonceStr, // 支付签名随机串，不长于 32 位
            'package': data.pack, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
            signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
            paySign, // 支付签名
            success: function (res) {
                // 支付成功后的回调函数
                cb(res);
            },
            fail: function (res) {
                errorCb(res);
            },
            cancel: function (res) {
                errorCb(res);
            }
        });

    });

    jweixi.error(function (res) {
        errorCb(res);
        // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
        // //alert("config信息验证失败");
    });
}

export default wexinPay;