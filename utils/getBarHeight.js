const systemInfo = uni.getSystemInfoSync()

const getBarHeight = {
    /**
     * 获取胶囊按钮位 置
     */
    getMenuPosition() {
        let top = 4
        let right = 7
        let width = 87
        let height = 32
        if (systemInfo.platform === 'devtools' && systemInfo.system.indexOf('Android') === -1) {
            top = 6
            right = 10
        } else if (systemInfo.platform === 'devtools' && systemInfo.system.indexOf('Android') != -1) {
            top = 8
            right = 10
        } else if (systemInfo.system.indexOf('Android') != -1) {
            top = 8
            right = 10
            width = 95
        }
        return {
            top: systemInfo.statusBarHeight + top,
            left: systemInfo.windowWidth - width - right,
            width: width,
            height: height
        }
    },

    /**
* 获取导航栏样式
*/
    getNavigationBarStyle(flag = true,flag1 = false) {
        
        let menuPosition = this.getMenuPosition()
        let navigationBarPosition = {
            "topHeight": systemInfo.statusBarHeight,
            "padding-top":systemInfo.statusBarHeight,
            height: (menuPosition.top - systemInfo.statusBarHeight) * 2 + menuPosition.height
        }

        if (flag1) {
            return navigationBarPosition;
        }

        if (flag) {
            uni.setStorageSync('navTrueHeight', navigationBarPosition["topHeight"] + navigationBarPosition.height);
            uni.setStorageSync('Navstyle', this.formatStyle(navigationBarPosition));
            return this.formatStyle(navigationBarPosition)

        } else {
            return navigationBarPosition["topHeight"] + navigationBarPosition.height
        }

    },
    /**
 * 格式化Style
 */
    formatStyle(position) {
        let styles = []
        for (let key in position) {
            styles.push(`${key}: ${position[key]}px;`)
        }
        return styles.join(' ')
    }
}

export default { getBarHeight };