export default {
    data() {
        return {
            year: "",
            month: "",
            day: "",
            yearData: [],
            monthData: [],
            dayData: [],
        }
    },
//获取当前时间的年月日
    getYearMounthDay() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        this.getDayData(year, month);
    },
    //获取年份----------年份是在当前年份向前60年向后10年
    getYearData(data) {
        let arr = new Array();
        for (let i = data - 60; i < data + 10; i++) {
            arr.push(i)
        }
        this.yearData = arr;
        this.yearIndex = arr.indexOf(this.year);
    },
    //获取月份
    getMonthData() {
        let arr = new Array();
        for (let i = 1; i < 13; i++) {
            arr.push(i)
        }
        this.monthData = arr;
    },
    //计算日期
    getDayData(year, month) {
        let day;
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            day = 31
        } else if (month == 4 || month == 6 || month == 11 || month == 9) {
            day = 30
        } else if (month == 2) {
            if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) { //闰年
                day = 29
            } else {
                day = 28
            }
        }
        let arr = new Array();
        for (let i = 1; i < day + 1; i++) {
            arr.push(i)
        }
        this.dayData = arr;
    },

    //年份变化时
    yearChange(picker, value, index) {
        this.year = value;
        this.getDayData(value, this.month);
    },
    //月份变化时
    monthChange(picker, value, index) {
        this.month = value;
        this.getDayData(this.year, value);
    }
}

