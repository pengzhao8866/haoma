import request from './request'
const getCommonTime = () => {
	Date.prototype.Format = function (fmt) { // author: meizz
		let o = {
			"M+": this.getMonth() + 1, // 月份
			"d+": this.getDate(), // 日
			"h+": this.getHours(), // 小时
			"m+": this.getMinutes(), // 分
			"s+": this.getSeconds(), // 秒
			"q+": Math.floor((this.getMonth() + 3) / 3), // 季度
			"S": this.getMilliseconds() // 毫秒
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		for (let k in o)
			if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" +
				o[k]).substr(("" + o[k]).length)));
		return fmt;
	}
}

// 单个倒计时时
const getTime = (time, num, type = 'd', callback) => {
	let ordertime = '';
	//获取最后提交日期
	let endDate = '';

	if (type != 'none') {
		if (time) {
			ordertime = new Date(time);
		} else {
			ordertime = new Date();
		}

		switch (type) {
			case 'd':
				endDate = new Date(ordertime.setDate(ordertime.getDate() + num));
				break;
			case 'h':
				endDate = new Date(ordertime.setHours(ordertime.getHours() + num));
				break;
			case 'm':
				endDate = new Date(ordertime.setMinutes(ordertime.getMinutes() + num));
			case 's':
				endDate = new Date(ordertime.setSeconds(ordertime.getSeconds() + num));
				break;
			default:
				break;
		}
	} else {
		endDate = new Date(time + (new Date()).getTime())
	}

	function p(n) {
		return n < 10 ? '0' + n : n;
	}
	let realTimeClData = setInterval(() => {
		//获取当前时间
		let nowDate = new Date();
		let countDown = ''

		countDown = (endDate.getTime() - nowDate.getTime()) / 1000;


		//关闭定时器
		if (countDown <= 0) {

			clearInterval(realTimeClData);

			callback({
				success: false
			});
		} else {
			let d = p(parseInt(countDown / (24 * 60 * 60, 10)));
			let h = p(parseInt(countDown / (60 * 60) % 24, 10));
			let m = p(parseInt(countDown / 60 % 60, 10));
			let s = p(parseInt(countDown % 60, 10));
			const res = {
				d,
				h,
				m,
				s,
				success: true
			}
			callback(res)
		}

	}, 1000)
}

//多个列表倒计时
// query ={
// 	key:"payTime",
// 	type:'status',
// 	typeNum:10,
// 	endTypeNum:90,
// 	callback: res=>{
// 		this.setData({
// 			List: res
// 		})
// 	}
// })

const getTimeList = (List, query) => {

	let nowDate = new Date();
	getTimeListEvent(List, query, nowDate);
	let realTimeClData = setInterval(() => {
		getTimeListEvent(List, query, nowDate)
	}, 1000)
	return realTimeClData
}

const getTimeListEvent = (List, query, nowDate) => {
	let nowDate1 = new Date();
	let endDate = '';
	let ordertime = '';
	List.forEach(element => {

		if (element[query.type] == query.typeNum) {
			if (query.isEndTime) {
				ordertime = new Date(element[query.key]);
				endDate = new Date(ordertime.setDate(ordertime.getDate() + query.num));
			} else {
				ordertime = element[query.key];
				endDate = new Date(nowDate.getTime() + ordertime);
			}
			let countDown = (endDate.getTime() - nowDate1.getTime()) / 1000;
			let d = p(parseInt(countDown / (24 * 60 * 60)));
			let h = p(parseInt(countDown / (60 * 60) % 24));
			let m = p(parseInt(countDown / 60 % 60));
			let s = p(parseInt(countDown % 60));
			if (countDown <= 0) {
				element[query.type] = query.endTypeNum;
			}
			element.RemainingTime = { d, h, m, s }
		}
	})
	query.callback(List)
}

function p(n) {
	return n < 10 ? '0' + n : n;
}

const arrayChage = (list) => {

	list.forEach(element => {

		if (element.jumpType == 3) {
			element.jumpUrl = `../missionDetail/missionDetail?id=${element.itemId}`
		} else if (element.jumpType == 4) {
			element.jumpUrl = '../getMember/getMember'
		}
		// #ifdef H5
		else if (element.jumpUrl.indexOf('h5-replace') > -1) {
			element.jumpType = 5
		}
		// #endif


	});
	return list
}

const getTrim = () => {
	String.prototype.trim = function () {
		return this.replace(/^\s+|\s+$/g, '');
	}
}

//清除本地一些缓存
const delwxStorage = (type = 1) => {
	if (uni.getStorageSync("redpackageItem")) {
		wx.removeStorage({
			key: 'redpackageItem'
		});
		wx.removeStorage({
			key: 'redpackageIndex'
		});
	}
	if (uni.getStorageSync("orderDeatil")) {
		wx.removeStorage({
			key: 'orderDeatil'
		});
	}

	if (uni.getStorageSync("goback")) {
		wx.removeStorage({
			key: 'goback'
		});
	}
	if (type != 2) {
		if (uni.getStorageSync("membergobackgoodId")) {
			wx.removeStorage({
				key: 'membergobackgoodId'
			});
		}
	}

}



//解决小程序iphoneX手机底部的兼容问题 https://blog.csdn.net/qq_43071910/article/details/85294434
const isPhonex = () => {
	return new Promise((resolve, reject) => {
		// #ifndef H5
		uni.getSystemInfo({
			success: res => {
				// console.log('手机信息res'+res.model)
				let modelmes = res.model;
				if (modelmes.indexOf('iPhone X') > -1) {
					resolve(true)
				} else {
					resolve(false)
				}
			}
		})
		//#endif
		// #ifdef H5
		var u = navigator.userAgent;
		var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
		if (isIOS) {
			if (screen.height == 812 && screen.width == 375) {
				resolve(true)
			} else {
				resolve(false)
			}
		} else {
			resolve(false)
		}
		//#endif
	})
}

//得到页面的相关信息
const systemInfo = () => {
	return new Promise((resolve, reject) => {
		uni.getSystemInfo({
			success: res => {
				resolve(res)
			}
		})
	})
}

//获取query json化，id不传时，获取全部
const urlToJson = (key, url) => {
	var theRequest = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
		}
	}
	return key ? theRequest[key] : theRequest;
}

//传参处理
const queryChange = query => {
	let shub = query.split("-")
	let shuc = {};
	shub.forEach(res => {
		let shud = res.split(":");
		shuc[shud[0]] = shud[1]
	})
	return shuc
}

//字符串长度到达一个阈值时，显示省略符号
const strOmit = (str, num = 10) => {
	let s = str;
	if (str.length > num) {
		s = str.substring(0, 10) + "...";
	}
	return s
}

//得到是否是ios环境
const isIos = () => {
	return new Promise((resolve, reject) => {
		// #ifdef H5
		resolve(false);
		// #endif
		// #ifndef H5
		// resolve(true);
		// return;
		uni.getSystemInfo({
			success: function (res) {

				if (res.platform == "ios") {
					uni.setStorageSync("isIos", true);
					resolve(true);
				} else {
					uni.setStorageSync("isIos", false);
					resolve(false)
				}
			}
		})
		// #endif
	})
}

const isIos1 = () => {
	return new Promise((resolve, reject) => {
		// resolve(true);
		// 		return;
		uni.getSystemInfo({
			success: function (res) {

				if (res.platform == "ios") {
					uni.setStorageSync("isIos", true);
					resolve(true);
				} else {
					uni.setStorageSync("isIos", false);
					resolve(false)
				}
			}
		})
	})
}



//得到位置授权
const getLocation = () => {
	var that = this;
	//1、获取当前位置坐标
	uni.getLocation({
		type: 'wgs84',
		success: function (res) {
			uni.setStorageSync('latitude', res.latitude);
			uni.setStorageSync('longitude', res.longitude);
		}
	})
}


const formateObjToParamStr = (paramObj) => {
	function filter(str) { // 特殊字符转义
		str += ''; // 隐式转换
		str = str.replace(/%/g, '%25');
		str = str.replace(/\+/g, '%2B');
		str = str.replace(/ /g, '%20');
		str = str.replace(/\//g, '%2F');
		str = str.replace(/\?/g, '%3F');
		str = str.replace(/&/g, '%26');
		str = str.replace(/\=/g, '%3D');
		str = str.replace(/#/g, '%23');
		return str;
	}
	const sdata = [];
	for (let attr in paramObj) {
		sdata.push(`${attr}=${filter(paramObj[attr])}`);
	}
	return sdata.join('&');
};

//对后端返回的不可发货的地址进行处理
const getLimitArea = (data) => {
	let notSupportCity = [];
	data.advierft = true
	if (data.province) {

		let province = []
		for (const key in data.province) {
			const element = data.province[key];
			if (data.provinceId && key == data.provinceId) {

				data.advierft = false
			}
			province.push({
				"provinceId": key,
				"pname": element
			})
			notSupportCity.push(element + '全境')
		}
		data.province = province;

	}

	if (data.city) {
		let city = []
		for (const key in data.city) {
			const element = data.city[key];
			if (data.cityId && key == data.cityId) {
				data.advierft = false
			}
			city.push({
				"cityId": key,
				"cityName": element
			})
			notSupportCity.push(element)
		}
		data.city = city
	}
	data.notSupportCity = notSupportCity.join('，')
	return data
}

//对跳转进行处理
const goLink =(url) =>{
	if (!url) {
		return ;	
	}
	if (url.indexOf('http') > -1 || url.indexOf('www') > -1) {
		uni.navigateTo({
			url: `../h5Page/h5Page?h5Link=${encodeURIComponent(url)}`,
		});
	}else if(url.indexOf("#") > -1){
		uni.switchTab({
			url: `${url.replace('#pages','..')}`,
		});
	}else{
		uni.navigateTo({
			url: `${url.replace('pages','..')}`,
		});
	}
}

export {
	getCommonTime,
	getTime,
	arrayChage,
	getTrim,
	delwxStorage,
	isPhonex,
	urlToJson,
	queryChange,
	strOmit,
	isIos,
	isIos1,
	getLocation,
	formateObjToParamStr,
	systemInfo,
	getTimeList,
	getLimitArea,
	goLink
}
