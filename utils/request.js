// #ifdef H5
import { h5_goto, ifweixin } from "./h5_util";
// #endif

let commonUrl = "http://47.100.76.52:8080/" //开发版
// let commonUrl = "https://test-api.hbei.vip/"; //体验版
// let commonUrl = "https://api.hbei.vip/"; //正式版
// 

const handlerPost = (
  url,
  data,
  type = "POST",
  IsLoad = true,
  IsErrorTip = true
) => {

  let promise = new Promise((resolve, reject) => {
      if (!url) {
        resolve({
          code : 200
        })
        return;
      }
    let success = false;
    if (IsLoad) {
      setTimeout(() => {
        if (!success) {
          uni.showLoading({
            title: "加载中"
          });
        }
        setTimeout(() => {
          if (!success) {
            // uni.hideLoading();
          }
        }, 2000);
      }, 400);
    }
    data = JSON.stringify(data)
    uni.request({
      url: commonUrl + url,
	  // url:'/api/'+url,
      data,
      method: type,
      dataType: 'json',
      header: {
        'Content-Type': 'application/json;charset=UTF-8',
        "Access-Control-Allow-Origin": "*",
        "JC-AUTHORIZATION": uni.getStorageSync("token") || ''
      },
      success: res => {
        if (res.data.code == 200) {
          success = true;
          uni.hideLoading();
        } else {
          success = true;
          if (IsErrorTip) {
            uni.showToast({
              title: res.data.data,
              icon: "none",
              duration: 1500
            });
          }
        }
        if (res.data.code == "401") {
          recordRouter()
          setTimeout(() => {
            
            uni.redirectTo({
              url: "../login/login"
            });
          }, 1000);
        }

        resolve(res.data);
      },
      error: function (e) {
        reject(e);
      }
    });
  });
  return promise;
};


//记录当前页面路由路径及参数
const recordRouter = router => {
  if (router) {
    uni.setStorageSync("router", router);
    return;
  }
  //  #ifndef H5
  let pages = getCurrentPages();
  let currPage = null;
  if (pages.length) {
    currPage = pages[pages.length - 1];
  }
  let query = "";
  if (currPage.options) {
    for (const key in currPage.options) {
      query += key + "=" + currPage.options[key] + "&";
    }
    query = query.substr(0, query.length - 1);
  }
  currPage.route = currPage.route.replace("pages", "..");

  uni.setStorageSync("router", `${currPage.route}?${query}`);
  //#endif

  // #ifdef H5
  let PageRouter = window.location.href.split("/pages/")[1];
  uni.setStorageSync("router", `../${PageRouter}`);
  // #endif
  if (!wx.getStorageSync("token")) {
    // wx.hideTabBar();
    wx.redirectTo({
      url: '../login/login',
    });

  }
};

const gotoRouter = () => {
  let router = wx.getStorageSync("router") || '../index/index'
  wx.navigateTo({
    url: router,
  });
  wx.switchTab({
    url: router,
  });
}




//得到个人资料
const getUsetInfo = (token) => {
  let promise = new Promise((resolve, reject) => {

    if (uni.getStorageSync("token") || token) {
      handlerPost("user/getBaseInfo", {}, 'GET').then(res => {
        if (res.code == 200) {

          uni.setStorageSync("userInfo", res.data);
          //  #ifdef H5
          // if (res.data.h5OpenId && ifweixin()) {
          //   wx.setStorageSync("H5session_key", res.data.h5OpenId);
          // }
          // #endif

        } else {
          if (uni.getStorageSync("userInfo")) {
            uni.removeStorageSync("userInfo");
          }
          if (uni.getStorageSync("token")) {
            uni.removeStorageSync("token");
          }
          getSession_key();
        }
        resolve(res);
      });
    } else {
      resolve("");
    }
  });
  return promise;
};






//获得session-id
const getSession_key = () => {
  uni.login({
    success: r => {
      // console.log(r.code)
      // return ;
      handlerPost(
        "account/weChat/getSessionKeyByCode",
        { code: r.code },
        "POST"
      ).then(res => {
        if (res.success) {
          uni.setStorageSync("session_key", res.data.sessionKey);
        }
      });
    }
  });
};


export default {
  handlerPost,
  recordRouter,
  getUsetInfo,
  getSession_key,
  gotoRouter
};
