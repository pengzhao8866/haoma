/**
 * 微信js-sdk
 * 参考文档：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141115
 */
import jweixin from 'jweixin-module'
import request from './request.js'
import { isIos1 } from './util.js'

//分享
const wxApi = {

    /**
    * [wxRegister 微信Api初始化]
    * @param  {Function} callback [ready回调函数]
    */
    async  wxRegister(callback, flag = false) {
        let url = window.location.href
        if (await isIos1()) {
            if (window.initurl) {
                url = window.initurl
            } else {
                window.initurl = window.location.href
            }
        }
        return new Promise((resolve, reject) => {

            // 这边的接口请换成你们自己的
            request.handlerPost('account/weChat/getSignature', {
                url: encodeURI(url)
            }).then(res => {
                let data = res.data
                jweixin.config({
                    debug: flag ? true : false, // 开启调试模式
                    appId: data.appId, // 必填，公众号的唯一标识
                    timestamp: data.timestamp, // 必填，生成签名的时间戳
                    nonceStr: data.nonceStr, // 必填，生成签名的随机串
                    jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone', "getLocation", "openLocation"], // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                    signature: data.signature, // 必填，签名，见附录1
                })
            })

            jweixin.ready((res) => {
                // 如果需要定制ready回调方法
                if (callback) {
                    resolve(callback())
                }
            })
        })
    },
    /**
    * [ShareTimeline 微信分享到朋友圈]
    * @param {[type]} option [分享信息]
    * @param {[type]} success [成功回调]
    * @param {[type]} error   [失败回调]
    */
    ShareTimeline(option) {
        jweixin.onMenuShareTimeline({
            title: option.title, // 分享标题
            link: option.link || window.location.href, // 分享链接
            imgUrl: option.imgUrl, // 分享图标
            success() {
                // 用户成功分享后执行的回调函数
                option.success()
            },
            cancel() {
                // 用户取消分享后执行的回调函数
                option.error()
            }
        })
    },
    /**
    * [ShareAppMessage 微信分享给朋友]
    * @param {[type]} option [分享信息]
    * @param {[type]} success [成功回调]
    * @param {[type]} error   [失败回调]
    */
    ShareAppMessage(option) {
        return new Promise((resolve, reject) => {
            jweixin.onMenuShareAppMessage({
                title: option.title, // 分享标题
                desc: option.desc, // 分享描述
                link: option.link || window.location.href, // 分享链接
                imgUrl: option.imgUrl, // 分享图标
                success() {
                    // 用户成功分享后执行的回调函数
                    resolve(1)
                    if (option.success) {
                        option.success()
                    }
                },
                cancel() {
                    // 用户取消分享后执行的回调函数
                    option.error()
                }
            })
        })

    },

    //得到定位
    getLocation() {
        return new Promise((resolve, reject) => {
            jweixin.getLocation({
                type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                success(res) {
                    resolve(res)
                },
                fail: (res) => {
                    // if (uni.getStorageSync("nowCity")) {
                    //     uni.removeStorageSync("nowCity");
                    // }
                    resolve('')
                },
                cancel: res => {
                    // if (uni.getStorageSync("nowCity")) {
                    //     uni.removeStorageSync("nowCity");
                    // }
                    resolve('')
                }
            })
        })
    },
    openLocation(option) {
        jweixin.openLocation({
            longitude: option.longitude,
            latitude: option.latitude,
            name: option.name,
            address: option.address
        });
    }
    //打开微信内置地图



}


export default wxApi