import request from './request'
import regeneratorRuntime from '../lib/regenerator-runtime'
import { isIos1 } from "./util";
var app = getApp();
let browser = {
    versions: function () {
        let u = navigator.userAgent, app = navigator.appVersion;
        // alert(u)
        return {         //移动终端浏览器版本信息
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
            iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            QQbrw: u.indexOf('MQQBrowser') > -1 && u.indexOf(" QQ") < 0 && !(u.indexOf('MicroMessenger') > -1), // QQ浏览器
            qq: u.indexOf(' QQ') > -1 && u.indexOf('MQQBrowser') < 0, // QQ
            weixin: u.indexOf('MicroMessenger') > -1
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
}

//判断是否路由栈的长度大于一，如果有则返回上一级，没有则返回首页

const H5goback = async (flag = false) => {
    if (flag) {
        let isIos = await isIos1()
        if (isIos) {
            window.location.href = document.referrer;
            return false;
        }
        window.history.go(-1);
    } else {
        if (getCurrentPages().length > 1) {
            uni.navigateBack({
                delta: 1
            });
        } else {
            uni.switchTab({
                url: '../index/index'
            })
        }
    }

}

//返回host
const returnHost = () => {
    let host = ''
    if (window.location.host.indexOf("test") > -1) {
        host = `https://test-h5.hbei.vip/mall/`
    } else if (window.location.host.indexOf("dev") > -1 || window.location.host.indexOf("8080") > -1) {
        host = `https://dev-h5.hbei.vip/mall/`
    }
    else {
        host = `https://h5.hbei.vip/mall/`
    }
    return host
}

// 判断是否为微信浏览器内
const ifweixin = () => {
    let flag = false
    if (browser.versions.mobile) {//判断是否是移动设备打开。browser代码在下面

        var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
        if (ua.match(/MicroMessenger/i) == "micromessenger") {
            //在微信中打开   
            flag = true;
        }
    } else {
        //否则就是PC浏览器打开
        flag = false
    }
    return flag
}

//判断是否是安卓下的qq浏览器
const isAndroidQq = () => {
    let flag = false
    if (browser.versions.QQbrw && !browser.versions.ios) {
        flag = true
    }
    return flag
}

//h5跳确认订单页面
const h5_goto = async (res,  type) => {
   
    let isIos = await isIos1()
    let path = type == 4 ? 'orderDetail/orderDetail' : "makeSureOrder/makeSureOrder"

    if (!ifweixin() || (uni.getStorageSync('H5session_key') && !isIos)) {
        if (!isIos) {
            uni.navigateTo({
                url: `../${path}?${res}`
            });
        } else {
            uni.redirectTo({
                url: `../${path}?${res}`
            });
        }
    } else {
        
        let url = '';
        if (window.location.host.indexOf("dev") > -1 || window.location.host.indexOf("8080") > -1) {
            url = `https://dev-h5.hbei.vip/mall/pages/${path}`
        }
        else if (window.location.host.indexOf("test") > -1) {
            url = `https://test-h5.hbei.vip/mall/pages/${path}`
        } else {
            url = `https://h5.hbei.vip/mall/pages/${path}`
        }

        if (isIos && uni.getStorageSync('H5session_key')) {
            let json = queryToJson(res)
            uni.setStorageSync('query', json);
            window.location.href = `${url}?${res}&h5goback=true`
        } else {
            let json = queryToJson(res);
            uni.setStorageSync('query', json);
            url = encodeURIComponent(url);
            window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx997563739c04a226&redirect_uri=${url}&response_type=code&scope=snsapi_base&state=12#wechat_redirect`;
        }

    }
}

//query参数改为json格式
const queryToJson = (res) => {
    let arr = res.split("&")
    let obj = {};
    for (let i of arr) {
        obj[i.split("=")[0]] = i.split("=")[1];  //对数组每项用=分解开，=前为对象属性名，=后为属性值
    }
    return obj
}

//h5 多个标题吸顶效果实现
const h5sticky = (flag) => {
    if (!flag) {
        return;
    }
    var box = document.getElementsByClassName('i-sticky-item-header'),  //获取所有需要吸顶效果的标题
        section = document.getElementsByClassName('i-sticky-item'); //获取所有子列表
    var ot = [],              //存储每个标题的offsetTop
        len = box.length;     //标题的个数

    for (let i = 0; i < len; i++) {
        ot.push(box[i].offsetTop);   //获取每个标题的offsetTop
    }

    //获取最后一个子列表的高度，为了设置最后一个吸顶标题的位置
    //section[len-1].getBoundingClientRect().height
    //此方法返回一个number

    ot.push(box[len - 1].offsetTop + section[len - 1].getBoundingClientRect().height);

    window.onscroll = function () {
        //获取滚动的高度
        let st = document.documentElement.scrollTop || document.body.scrollTop;
        for (let i = 0; i < len; i++) {
            if (st > ot[i] && st < ot[i + 1]) {  //滚动时监听位置，为标题的吸顶设置一个现实范围
                box[i].className = 'i-sticky-item-header i-class  i-sticky-fixed ';
            } else {
                box[i].className = 'i-sticky-item-header i-class ';
            }

        }
    }
}

//页面是否固定
const fixed = (passive = true) => {
    setTimeout(() => {
        console.log(document.querySelector('.mask'));
        document.querySelector('.mask').addEventListener('touchmove', function (e) {
            e.preventDefault();
        });
    }, 300);

}

//得到图片地址前缀
//返回host
const returnHost1 = () => {
    let host = "";
    if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("dev") > -1
    ) {
        host = `http://106.12.176.120:15200/`;
    } else if (window.location.host.indexOf("test") > -1 || window.location.host.indexOf("8080") > -1) {
        host = `https://test-api.hbei.vip/account`;
    } else {
        host = `https://api.hbei.vip/account`;
    }
    return host;
}


//得到验证图片地址


import { formateObjToParamStr } from "./util";

const VerifyImg1 = (phone) => {

    let query = formateObjToParamStr({
        phone
    });
    return `${returnHost1()}/app/user/login/getVerifyPicture?${query}`;
}

export {
    h5_goto,
    queryToJson,
    ifweixin,
    returnHost,
    H5goback,
    h5sticky,
    fixed,
    VerifyImg1,
    isAndroidQq
}